# Shader Graph viewer

This is a simple tool to browse, submit and share shader graphs made in Unity's [Shader Graph](https://unity.com/shader-graph) editor right from your browser.

The tool has been built from scratch using Vanilla JS and is currently hosted on the static website service Netlify.

You can access the live version here: [https://shadergraph.stelabouras.com/](https://shadergraph.stelabouras.com/).

The icons are provided by [Tabler Icons](https://github.com/tabler/tabler-icons).

**Mouse shortcuts**: Left or Middle mouse click to pan, mouse wheel to zoom.

This is an early version, so here's a short list of what works and what not:

## Features

* Local preview of shadergraphs
* Blackboard
* Pan and resize
* Inspector for custom functions
* Expandable nodes
* Groups
* Sticky notes
* Move nodes, groups and sticky notes
* Fullscreen support
* Tested on Firefox, Opera, Safari, Chrome
* Shader Graph Library
* Preview links
* Embed feature

### Experimental features

* [Slot dimensions](https://github.com/Unity-Technologies/Graphics/blob/fbb4462b6602543a8513c2a88def1c014c076e69/com.unity.shadergraph/Editor/Data/Graphs/MaterialSlot.cs)

## TODO

* [Color Modes](https://docs.unity3d.com/Packages/com.unity.shadergraph@6.9/manual/Color-Modes.html) support
* Better [Gradient node](https://docs.unity3d.com/Packages/com.unity.shadergraph@6.9/manual/Gradient-Node.html) support
* Mobile friendly
* Multiple node selection (drag to select)

### Awesome if implemented

* Live previews
* [Shadertoy](https://www.shadertoy.com/)/[GSN Composer](https://www.gsn-lib.org/) integration

## Run locally

To run the server locally, install `netlify-cli` tool, `cd build/` and do `netlify dev -p 8888`.

You can also generate the library locally by installing `virtualenv` then the required python libraries (`requirements.txt`) and on the root folder run `python src/build_library.py`.

### License

Shader Graph Viewer is licensed under the [GNU License](https://gitlab.com/stelabouras/shader-graph/-/blob/master/LICENSE).
