'use strict';

var crypto = require('crypto');
var fs = require('fs');
var request = require("request");

// populate environment variables locally.
require('dotenv').config()

/*
  Our serverless function handler
*/
export function handler(event, context, callback) {

  console.log(event);
  // get the arguments from the notification
  var body = JSON.parse(event.body);

  console.log(body);

  var shadergraphName = body.data.shadergraphname;
  var uploaderName = body.data.uploadername;
  var uploaderEmail = body.data.uploaderemail;
  var isSubgraph = body.data.shadergraphsub;
  var subgraphGUID = body.data.shadergraphguid;
  var tags = body.data.tags;

  var name = body.data.name;
  var id = body.id;

  var md5sum = crypto.createHash('md5');
  md5sum.update(uploaderEmail + '|' + id);

  var shadergraphSlug = md5sum.digest('hex');
  var shadergraphLink = "https://shadergraph.stelabouras.com/library/" + shadergraphSlug

  console.log(shadergraphLink);

  var emailBody = "Hey "  + (uploaderName.length == 0 ? "there" : uploaderName) + ",\n"
  emailBody += "\nYour shadergraph '" +  shadergraphName + "' was just published!\n\n"
  emailBody += "Visit the link below to view it: "
  emailBody += shadergraphLink
  emailBody += "\n\nCheers,\nStelios"

  // prepare call to the Slack API
  var shaderGraphsFormId = process.env.SHADERGRAPHS_FORM_ID;
  var slackURL = process.env.SLACK_WEBHOOK_URL
  var slackPayload = {
    "text": "New shadergraph!",
    "attachments": [
      {
        "fallback": "New shadergraph",
        "color": "#444",
        "title": shadergraphName,
        "title_link": 'https://app.netlify.com/sites/shader-graph-viewer/forms/' + shaderGraphsFormId + '#item-' + id,
        "text": 'Uploader email: ' + uploaderEmail + '\nUploader name: ' + (uploaderName.length == 0 ? 'Not provided' : uploaderName) + '\nTags: ' + (tags.length == '' ? 'Not provided' : tags) + '\nIs subgraph: ' + isSubgraph + (isSubgraph == '1' ? '\nSubgraph GUID : ' + (subgraphGUID.length == 0 ? 'Not provided' : subgraphGUID) : '')
      },
      {
        "fallback": "Manage shadergraph on " + process.env.URL,
        "callback_id": "shadergraph-action",
        "actions": [
          {
            "type": "button",
            "text": "Approve",
            "name": "approve",
            "value": body.id
          },
          {
            "type": "button",
            "style": "danger",
            "text": "Delete",
            "name": "delete",
            "value": body.id
          },
          {
            "type": "button",
            "text": "Email user",
            "url": "mailto:" + uploaderEmail + "?subject=Shadergraph%20published!&body=" + encodeURIComponent(emailBody)
          }
        ]
      }]
    };

    console.log(slackPayload);

    // post the notification to Slack
    request.post({url:slackURL, json: slackPayload}, function(err, httpResponse, body) {
      var msg;
      if (err) {
        msg = 'Post to Slack failed:' + err;
      } else {
        msg = 'Post to Slack successful!  Server responded with:' + body;
      }
      callback(null, {
        statusCode: 200,
        body: msg
      })
      return console.log(msg);
    });
}