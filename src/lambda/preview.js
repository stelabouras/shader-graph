exports.handler = async function(event, context) {
  const encrypted = event.queryStringParameters?.encrypted ?? "";
  console.log(`Encrypted body: ${encrypted}`);
  console.log(`previews form id: ${process.env.PREVIEWS_FORM_ID}`);
  const submissionsUrl = `https://api.netlify.com/api/v1/forms/${process.env.PREVIEWS_FORM_ID}/submissions/`
  try {
    const response = await fetch(submissionsUrl, {
      method: 'GET',
      headers: {
        'Authorization': `Bearer ${process.env.API_AUTH}`,
      },
    });

    if (response.ok) {
      const submissions = await response.json();
      var submissionId = '';
      for (var i = 0; i < submissions.length; i++) {
        const submission = submissions[i];
        console.log(submission);
        console.log(submission['data']['shadergraph']);
        if (submission['data']['shadergraph'].startsWith(encrypted)) {
          console.log(`FOUND IT! ${submission['id']}`);
          submissionId = submission['id'];
          break;
        }
      }
      console.log(`submissionId ${submissionId}`);
      if (submissionId.length > 0) {
        return {
          statusCode: 200,
          body: JSON.stringify({ message: `${submissionId}` })
        };
      }
      else {
        return {
          statusCode: 404,
          body: JSON.stringify({ message: `Submission not found!` })
        };
      }
    } else {
      console.error('Request failed with status:', response.status);
      return {
        statusCode: response.status,
        body: JSON.stringify({ message: `Error fetching submissions ${response.status}` })
      };
    }
  } catch (error) {
    console.error('Error:', error);
    return {
      statusCode: 500,
      body: JSON.stringify({ message: `Error fetching submissions: ${error}` })
    };
  }
};
