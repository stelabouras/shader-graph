import requests
import os
from os import path
from os.path import join
import hashlib
import shutil
import json

submissions_url = 'https://api.netlify.com/api/v1/forms/' + os.environ['SHADERGRAPHS_FORM_ID'] + '/submissions/'
response = None

try:
	response = requests.get(submissions_url, headers={'Authorization': 'Bearer ' + os.environ['API_AUTH']})
except KeyboardInterrupt:
	print('Interrupted')
	try:
		sys.exit(0)
	except SystemExit:
		os._exit(0)           
except:
	pass

outputFolder = 'build/library'

if path.exists(outputFolder):
	shutil.rmtree(outputFolder)

library = {}
submissions = response.json()

if len(submissions) == 0:
	print('No submissions found!')
	os._exit(0)   

subgraphs = {}

for submission in submissions:
	name = submission['data']['shadergraphname']
	uploaderemail = submission['data']['uploaderemail']
	uploadername = submission['data']['uploadername'] or None
	isSubgraph = submission['data']['shadergraphsub'] == '1'
	tags = submission['data']['tags']
	subgraphId = None
	if isSubgraph == True:
		subgraphId = submission['data']['shadergraphguid'].strip()

	shadergraphId = submission['id']
	shadergraphBody = submission['data']['shadergraphbody']
	created = submission['created_at']

	uniqueId = (uploaderemail + '|' + shadergraphId).encode('utf-8')
	shadergraphPath = hashlib.md5(uniqueId).hexdigest()

	library[shadergraphPath] = {
		'name': name,
		'email': uploaderemail,
		'uploadername' : uploadername,
		'isSubgraph': isSubgraph,
		'shadergraphId' : shadergraphId,
		'shadergraphBody': shadergraphBody,
		'tags': tags
	}

	if isSubgraph == True:

		if uploaderemail not in subgraphs:
			subgraphs[uploaderemail] = {}

		subgraphs[uploaderemail][subgraphId] = shadergraphPath

###

for shadergraphPath in library:

	shadergraph = library[shadergraphPath]
	outputPath = path.join(outputFolder, shadergraphPath)

	if not path.exists(outputPath):
		os.makedirs(outputPath)

	filePath = path.join(outputPath, 'index.html')

	contents = json.dumps(shadergraph['shadergraphBody'])

	shaderGraphfilename = shadergraph['name'] + '.' + ('shadersubgraph' if shadergraph['isSubgraph'] else 'shadergraph')
	shaderGraphfilePath = path.join(outputPath, shaderGraphfilename)

	print('Building ' + shadergraph['name'] + ' to ' + filePath)

	f = open(filePath, 'w+')

	html = """<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<title>""" + shadergraph['name'] + """ | Shader Graph Viewer</title>
		<link rel='stylesheet' href='/style/style.css'/>
		<link rel="apple-touch-icon" sizes="57x57" href="/icons/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="/icons/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="/icons/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="/icons/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="/icons/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="/icons/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="/icons/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="/icons/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="/icons/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="/icons/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="/icons/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="/icons/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="/icons/favicon-16x16.png">
		<link rel="manifest" href="/icons/manifest.json">
		<meta name="msapplication-TileColor" content="#1D1D1D">
		<meta name="msapplication-TileImage" content="/icons/ms-icon-144x144.png">
		<meta name="theme-color" content="#1D1D1D">
	</head>
	<body class="page-libraryitem">
		<header>
			<div id="zoom"></div>
			<h1>""" + shadergraph['name'] + """</h1>
			<div class="tools">
				<span><a href="/" id="create">Create new</a></span>
				<button title="Download file" id="download"><em class="download"></em>Download</button>
				<button title="Embed" id="embed"><em class="embed"></em>Embed</button>
				<button title="Library" id="library"><em class="library"></em>Library</button>
				<button title="Present blackboard" id="blackboard" disabled>Blackboard</button>
				<button title="Present Graph Inspector" id="graph-inspector" disabled>Graph Inspector</button>
				<button title="Reset position" id="reset" disabled><em class="reset"></em></button>
				<button title="Toggle fullscreen" id="fullscreen" disabled><em class="fullscreen"></em></button>
			</div>
		</header>
		<div id="canvas">			
			<div class="container"></div>
		</div>
		<div id="blackboard-container">
			<h5>Test</h5>
			<div class="property-list">
				<h6>Properties</h6>
				<ul></ul>
			</div>
		</div>
		<div id="inspector" class="close">
			<h5>Graph Inspector</h5>
			<nav>
				<ul>
					<li data-type="node">Node Settings</li>
					<li data-type="graph" class="active">Graph Settings</li>
				</ul>
			</nav>
			<div class="body">
			</div>
		</div>
		<div id="embed-container">
			<h5>
				Embed
				<em class="close"></em>
			</h5>
			<div class="body">
				<input type="text" value="<script>(function(d, s, id) {var js, shaderjs = d.getElementsByTagName(s)[0];if (d.getElementById(id)) return;js = d.createElement(s); js.id = id;js.src ='https://shadergraph.stelabouras.com/scripts/embed.js';shaderjs.parentNode.insertBefore(js, shaderjs);}(document, 'script', 'shadergraph-jssdk'));</script><div class='shadergraph-embed' data-shadergraphid='""" + shadergraphPath + """'></div>" readonly/>
				<button title="Copy to clipboard"><em class="copy"></em></button>
			</div>
		</div>
		<iframe src="/library/" class="close" id="library-container"></iframe>
	"""
	html += '<script>'

	if shadergraph['email'] in subgraphs:
		html += 'subgraphs = ' + json.dumps(subgraphs[shadergraph['email']]) + ';'
	else:
		html += 'subgraphs = {};'

	html += '</script>'

	html += '<script>\n'

	if isSubgraph:
		html += 'storedShaderGraphName = "' + shadergraph['name'] + '.shadersubgraph";\n'
	else:
		html += 'storedShaderGraphName = "' + shadergraph['name'] + '.shadergraph";\n'

	html += 'storedShaderGraphBody = ' + contents + ';\n'
	html += 'shaderGraphfilePath = "./' + shaderGraphfilename + '";'
	html += '</script>'
	html += """	
	<script type="text/javascript" src="/scripts/script.js"></script>
	</body>
</html>"""
	
	f.write(html.encode('utf-8').decode('utf-8'))
	f.close()

	print('Building ' + shadergraph['name'] + ' file to ' + shaderGraphfilePath)

	sf = open(shaderGraphfilePath, 'w+')
	sf.write(shadergraph['shadergraphBody'])
	sf.close()

	shaderEmbedfilePath = path.join(outputPath, 'embed.html')


	print('Building ' + shadergraph['name'] + ' embed to ' + shaderEmbedfilePath)

	ef = open(shaderEmbedfilePath, 'w+')

	eHtml = """<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<title>""" + shadergraph['name'] + """ | Shader Graph Viewer</title>
		<link rel='stylesheet' href='/style/style.css'/>
		<link rel="apple-touch-icon" sizes="57x57" href="/icons/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="/icons/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="/icons/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="/icons/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="/icons/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="/icons/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="/icons/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="/icons/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="/icons/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="/icons/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="/icons/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="/icons/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="/icons/favicon-16x16.png">
		<link rel="manifest" href="/icons/manifest.json">
		<meta name="msapplication-TileColor" content="#1D1D1D">
		<meta name="msapplication-TileImage" content="/icons/ms-icon-144x144.png">
		<meta name="theme-color" content="#1D1D1D">
	</head>
	<body class="page-embed">
		<header>
			<div id="zoom"></div>
			<h1><a href="/library/""" + shadergraphPath + """/" target="_blank">""" + shadergraph['name'] + """</a></h1>
			<div class="tools">
				<span><a href="/" target="_blank" id="create">Create new</a></span>
				<button title="Download file" id="download"><em class="download"></em>Download</button>
				<button title="Present blackboard" id="blackboard" disabled>Blackboard</button>
				<button title="Present Graph Inspector" id="graph-inspector" disabled>Graph Inspector</button>
				<button title="Reset position" id="reset" disabled><em class="reset"></em></button>
				<button title="Toggle fullscreen" id="fullscreen" disabled><em class="fullscreen"></em></button>
			</div>
		</header>
		<div id="canvas">			
			<div class="container"></div>
		</div>
		<div id="blackboard-container">
			<h5>Test</h5>
			<div class="property-list">
				<h6>Properties</h6>
				<ul></ul>
			</div>
		</div>
		<div id="inspector" class="close">
			<h5>Graph Inspector</h5>
			<nav>
				<ul>
					<li data-type="node">Node Settings</li>
					<li data-type="graph" class="active">Graph Settings</li>
				</ul>
			</nav>
			<div class="body">
			</div>
		</div>
	"""
	eHtml += '<script>'

	if shadergraph['email'] in subgraphs:
		eHtml += 'subgraphs = ' + json.dumps(subgraphs[shadergraph['email']]) + ';'
	else:
		eHtml += 'subgraphs = {};'

	eHtml += '</script>'

	eHtml += '<script>\n'

	if isSubgraph:
		eHtml += 'storedShaderGraphName = "' + shadergraph['name'] + '.shadersubgraph";\n'
	else:
		eHtml += 'storedShaderGraphName = "' + shadergraph['name'] + '.shadergraph";\n'

	eHtml += 'storedShaderGraphBody = ' + contents + ';\n'
	eHtml += 'shaderGraphfilePath = "./' + shaderGraphfilename + '";'
	eHtml += '</script>'
	eHtml += """	
	<script type="text/javascript" src="/scripts/script.js"></script>
	</body>
</html>"""

	ef.write(eHtml.encode('utf-8').decode('utf-8'))
	ef.close()
###

filePath = path.join(outputFolder, 'index.html')

if path.exists(filePath):
	shutil.rmtree(filePath)

f = open(filePath, 'w+')

html = """<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<title>Library | Shader Graph Viewer</title>
	<link rel='stylesheet' href='/style/style.css'/>
</head>
<body id="page-library">
	<h1>Library</h1>
	<section>
	<ul>
"""

for key in library:
	libraryItem = library[key]

	html += "<li"
	if len(libraryItem['tags']) > 0:
		html += " data-tags=\"" + libraryItem['tags'] + "\""
	html += " data-name=\"" + libraryItem['name'] + "\""
	html += " data-subgraph=\"" + ("1" if libraryItem['isSubgraph'] == True else "0") + "\""
	html += ">"
	html += "<a target=\"_parent\" href=\"/library/" + key + "/\">"
	html += libraryItem['name']
	html += "</a>"
	html += "</li>"

html +="""
	</ul>
	</section>
</body>
</html>"""

f.write(html.encode('utf-8').decode('utf-8'))
f.close()
