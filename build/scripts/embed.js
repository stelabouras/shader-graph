"use strict";

(function() {

    var ShaderGraphEmbed = {

        'initialize' : function() {

            [...document.querySelectorAll('.shadergraph-embed')].forEach((shadergraphElement) => {

                if(shadergraphElement.hasAttribute('data-shadergraphid'))
                    this.setup(shadergraphElement);
            });
        },

        'setup' : function(shadergraphElement) {

            var iframe = document.createElement('iframe');

            var shadergraphId = shadergraphElement.getAttribute('data-shadergraphid');
            var src = "https://shadergraph.stelabouras.com/library/";

            iframe.id = 'shadergraphid-' + shadergraphId;
            iframe.src = src + shadergraphId + '/embed.html';

            iframe.style.border = 'none';
            iframe.style.height = '100%';
            iframe.style.width = '100%';
            iframe.style.overflow = 'hidden';
            iframe.style.verticalAlign = 'bottom';

            iframe.setAttribute('allow', 'fullscreen');
            iframe.setAttribute('allowFullscreen', true);
            iframe.setAttribute('allowTransparency', true);
            iframe.setAttribute('frameBorder', 0);
            iframe.setAttribute('tabIndex', 0);
            iframe.setAttribute('scrolling', 'yes');

            shadergraphElement.appendChild(iframe);
        }
    };

    ShaderGraphEmbed.initialize();

})();
